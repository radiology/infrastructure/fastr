# Copyright 2011-2014 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
from pathlib import Path

import pytest

import fastr
from fastr import version

# Add option to the pytest parser
def pytest_addoption(parser):
    parser.addoption(
        "--runslow", action="store_true", default=False, help="run slow tests"
    )


# Skip tests marked with slow unless runslow argument was given
def pytest_collection_modifyitems(config, items):
    if config.getoption("--runslow"):
        # --runslow given in cli: do not skip slow tests
        return

    skip_slow = pytest.mark.skip(reason="need --runslow option to run")
    for item in items:
        if "slow" in item.keywords:
            item.add_marker(skip_slow)


def pytest_report_header(config):
    return f"fastr {version.version} - revision {version.git_revision} (branch {version.git_branch})"


@pytest.fixture(autouse=True, scope="session")
def fastr_dir(tmpdir_factory):
    tmp_dir = tmpdir_factory.mktemp('FastrTemp')
    fastr_home_dir = tmpdir_factory.mktemp('FastrHome')

    fastr.log.info('Using {} as the tmp for fastr and {} as FASTRHOME'.format(tmp_dir, fastr_home_dir))
    fastr_home_dir.join('config.py').write_text(
        "mounts['tmp'] = '{}'".format(tmp_dir),
        'utf-8'
    )
    os.environ['FASTRHOME'] = str(fastr_home_dir)

    # Override mounts field properly
    mounts = fastr.config.mounts
    mounts['tmp'] = str(tmp_dir)
    fastr.config.set_field('mounts', mounts)

    return Path(tmp_dir)


@pytest.fixture
def test_mount(tmpdir):
    mounts = fastr.config.mounts
    mounts['test'] = str(tmpdir)
    fastr.config.set_field('mounts', mounts)

    yield tmpdir

    mounts = fastr.config.mounts
    del mounts['test']
    fastr.config.set_field('mounts', mounts)


@pytest.fixture(autouse=True)
def deconfig_fastr():
    """
    We want all tests run with the fastr config deactivated, so import fastr
    ahead of the test and set some config fieds
    """
    fastr.log.info('Setting config to testing defaults')
    fastr.config.pim_host = ''  # Do not publish anything to PIM
    fastr.config.execution_plugin = "LinearExecution"  # This is the safest option
    fastr.config.source_job_limit = 0  # No need to limit in blocking option
    fastr.config.job_cleanup_level = 'no_cleanup'

