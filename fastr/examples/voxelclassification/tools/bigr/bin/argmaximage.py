import argparse
import nibabel
import numpy


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', metavar='IMAGE.nii.gz',
                        dest='input', type=str, required=True, nargs='+',
                        help='Images to perform argmax on')
    parser.add_argument('-o', '--output', metavar='OUTPUT.nii.gz',
                        dest='output', type=str, required=True,
                        help='Resulting output image')
    args = parser.parse_args()

    images = args.input
    print('Loading images: {}'.format(args.input))
    images = tuple(nibabel.load(x) for x in images)
    ref_affine = images[0].get_affine()
    images = numpy.array([x.get_data() for x in images])
    print('Loaded data array ({})'.format(images.shape))
    print('Getting maximum')
    result = numpy.apply_along_axis(numpy.argmax, 0, images).astype(numpy.int16)
    print('Creating output')
    output = nibabel.Nifti1Image(result, ref_affine)
    print('Writing results')
    nibabel.save(output, args.output)


if __name__ == '__main__':
    main()
