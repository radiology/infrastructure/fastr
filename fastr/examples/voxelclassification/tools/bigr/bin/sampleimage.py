#!/usr/bin/env python
import argparse
import random
import nibabel
import numpy


def number_spec(value):
    try:
        value = int(value)
        return value
    except ValueError:
        value = float(value)
        if not 0.0 < value <= 1.0:
            if int(value) == value:
                return int(value)
            else:
                raise ValueError('Fraction should be 0.0 < frac <= 1.0')
        return value


def main():
    # Parse arguments
    parser = argparse.ArgumentParser(description='Sample feature image (within a mask) and create a 2D feature array')
    parser.add_argument('-i', '--in', metavar='IMAGE.nii.gz', dest='image',
                        type=str, required=True, nargs='+',
                        help='The image to sample')
    parser.add_argument('-l', '--label', metavar='LABEL.nii.gz', dest='label',
                        type=str, required=False,
                        help='The label image of the regions to sample, '
                             'must be an integer image (max int16)')
    parser.add_argument('-m', '--mask', metavar='MASK.nii.gz', dest='mask',
                        type=str, required=False,
                        help='The mask to sample only part of an image (default is whole image)')
    parser.add_argument('-o', '--out', metavar='OUTPUT.npz', dest='output',
                        type=str, required=True,
                        help='The output arrays (numpy npz format)')
    parser.add_argument('-n', '--nsamples', metavar='N', dest='number',
                        type=number_spec, required=False, nargs='+',
                        help='Number of sample to take (default is all), can be int, float '
                             'or list of int/float. Int is an absolute number of samples, '
                             'whereas a float indicates a fraction (0.0 < frac <= 1.0). '
                             'A list must have same number as the number of classes in '
                             'the label image. If the number cannot be sampled, the highest '
                             'possible number of samples will be taken.')
    parser.add_argument('-s', '--seed', metavar='S', dest='seed',
                        type=int, required=False,
                        help='Random seed to used (default is system time)')
    parser.add_argument('-u', '--usenegative', dest='negative',
                        action='store_true', help='Sample negative classes (by default skip negative classes)')
    args = parser.parse_args()

    print('Arguments: {}'.format(args))

    # Do stuff
    print('Loading feature image')
    label = args.label
    mask = args.mask
    output = args.output
    number = args.number
    use_negative = args.negative

    # If no mask is supplied: use entire image
    print('Loading label image')
    label = nibabel.load(label).get_data().astype(numpy.int16)

    # Load input images
    images = []
    affine = []
    for feature_image in args.image:
        print('Loading input image {}'.format(feature_image))
        img = nibabel.load(feature_image)
        affine.append(img.get_affine())
        img = img.get_data().astype(numpy.float32, copy=False)
        if len(img.shape) == 5:
            images.append(img)
        elif len(img.shape) == 4:
            images.append(img.reshape(img.shape[:3] + (1,) + img.shape[3:]))
        elif len(img.shape) == 3:
            images.append(img.reshape(img.shape + (1, 1)))
        else:
            raise ValueError('Wrong feature size')

    if not all(numpy.array_equal(x, affine[0]) for x in affine):
        raise ValueError('Affine matrices of images are different, world coordinates do not match')

    # Concatenate images, remove old images stack
    image = numpy.concatenate(images, axis=4)
    del images

    if mask is not None:
        print('Loading mask image')
        mask = nibabel.load(mask).get_data().astype(numpy.bool)

    # If no number is given: use all samples in the mask
    if number is None:
        number = [1.0]

    # Set random seed (for reproducibility)
    random.seed(args.seed)

    # Classes
    classes = numpy.unique(label)
    print('Found classes: {}'.format(classes))

    # Number to sample
    if len(number) == 1:
        number *= len(classes)

    if len(number) != len(classes):
        message = ('Number of samples should be given for each class. I found'
                   ' {} classes in the label file and there are {} number of'
                   ' samples specified. No output samples will be'
                   ' produced.').format(len(classes), len(number))
        raise ValueError(message)

    # Sample each region
    samples = []
    targets = []
    for class_, nr_samples in zip(classes, number):
        # Skip negative class in needed
        if not use_negative and class_ < 0:
            print('Skipping negative class: {}'.format(class_))
            continue
        else:
            print('Sampling class {}'.format(class_))

        # The final mask to use to sample for this class (combine label and mask image)
        if mask is not None:
            class_mask = numpy.logical_and(mask, label == class_)
        else:
            class_mask = label == class_

        # Determine final number of samples required
        available_samples = numpy.sum(class_mask)
        if isinstance(nr_samples, float):
            print('Calculating sample number from fraction')
            nr_samples = int(nr_samples * available_samples)
        elif nr_samples > available_samples:
            print('More samples requested then available in the region (class {}), using entire region!'.format(class_))
            nr_samples = available_samples
        else:
            print('Keep using {} ({})'.format(nr_samples, type(nr_samples).__name__))

        # Take samples
        print('Taking {} of out {} samples'.format(nr_samples, available_samples))
        samples.append(numpy.squeeze(random.sample(image[class_mask], nr_samples)))
        targets.append(numpy.ones(nr_samples) * class_)

    # Merge samples into one set of arrays
    samples = numpy.concatenate(samples)
    targets = numpy.concatenate(targets).astype('int16')

    # Save results
    print('Saving results')
    numpy.savez_compressed(output, samples=samples, targets=targets)


if __name__ == '__main__':
    main()
