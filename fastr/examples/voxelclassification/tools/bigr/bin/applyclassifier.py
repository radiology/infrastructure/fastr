import argparse
import time

import nibabel
import numpy
from sklearn.externals import joblib


def label_type_check(value):
    options = ['uint8', 'uint16', 'uint32', 'uint64', 'int8', 'int16', 'int32', 'int64']
    if value not in options:
        raise ValueError('Label type should be in one of: {}'.format(options))
    return value


def apply_chunked(func, data, chunk_size=10000):
    number_of_chunks = int(numpy.ceil(len(data) / float(chunk_size)))

    print('Apply chunked on {} with chunk_size {}'.format(data.shape, chunk_size))
    output = []
    for chunk_nr, index in enumerate(range(0, len(data), chunk_size)):
        print('Processing chunk {} of {}'.format(chunk_nr + 1, number_of_chunks))
        t0 = time.time()
        output.append(func(data[index:index + chunk_size, ...]))
        t1 = time.time()
        print('Finished in {} seconds'.format(t1 - t0))

    return numpy.concatenate(output, axis=0)


def main():
    parser = argparse.ArgumentParser(description='Apply a classifier to an image')
    parser.add_argument('-c', '--classifier', metavar='CLASSIFIER.clf',
                        dest='classifier', type=str, required=True,
                        help='Trained classifier to apply to image')
    parser.add_argument('-i', '--image', metavar='IMAGE.nii.gz',
                        dest='image', type=str, nargs='+', required=True,
                        help='Image to apply the classifier to')
    parser.add_argument('-m', '--mask', metavar='MASK.nii.gz',
                        dest='mask', type=str, required=False,
                        help='Region of interest mask image')
    parser.add_argument('-l', '--label', metavar='LABEL.nii.gz',
                        dest='label', type=str, required=False,
                        help='Output label image')
    parser.add_argument('-p', '--probability', metavar='PROBABILITY.nii.gz',
                        dest='probability', type=str, required=False, nargs='+',
                        help='Output probability maps, one map per class')
    parser.add_argument('-n', '--nclasses', metavar='N',
                        dest='nclasses', type=int, required=False,
                        help='Number of classes in the image')
    parser.add_argument('-s', '--slots', '--cores', metavar='M', dest='cores',
                        type=int, required=False, default=1,
                        help='Number of cores to use for the RFC')
    parser.add_argument('-t', '--label_type', metavar='TYPE',
                        dest='label_type', type=label_type_check,
                        required=False, default='uint8')
    #parser.add_argument('--image_chunks', type=int, required=False,
    #                    dest='image_chunks', default=1, metavar='M',
    #                    help='Load image chunks to avoid high memory use')
    parser.add_argument('--classifier_chunk_size', type=int, required=False,
                        dest='classifier_chunk_size', default=10000, metavar='S',
                        help='Send data to classifier prediction in chunks to avoid high memory use')

    args = parser.parse_args()

    # Get args
    classifier_file = args.classifier
    image_files = args.image
    mask_file = args.mask
    label_file = args.label
    label_type = args.label_type
    probability_file = args.probability
    number_of_classes = args.nclasses
    number_of_cores = args.cores
    #image_chunks = args.image_chunks
    classifier_chunk_size = args.classifier_chunk_size

    # Load classifier
    print('Loading classifier {}'.format(classifier_file))
    classifier = joblib.load(classifier_file)

    if hasattr(classifier, 'n_jobs'):
        print('Changing classifier.n_jobs to {}'.format(number_of_cores))
        classifier.n_jobs = number_of_cores
    else:
        print('Classifier does not support n_jobs, ignoring number_of_cores={}'.format(number_of_cores))

    # Load input images
    images = []
    affine = []
    for feature_image in image_files:
        print('Loading input image {}'.format(feature_image))
        img = nibabel.load(feature_image)
        affine.append(img.get_affine())
        img = img.get_data().astype(numpy.float32, copy=False)
        if len(img.shape) == 5:
            images.append(img)
        elif len(img.shape) == 4:
            images.append(img.reshape(img.shape[:3] + (1,) + img.shape[3:]))
        elif len(img.shape) == 3:
            images.append(img.reshape(img.shape + (1, 1)))
        else:
            raise ValueError('Wrong feature size')

    if all(numpy.array_equal(x, affine[0]) for x in affine):
        affine = affine[0]
    else:
        raise ValueError('Affine matrices of images are different, world coordinates do not match')

    # Concatenate images, remove old images stack
    test_data = numpy.concatenate(images, axis=4)
    image_shape = test_data.shape
    del images

    # Load mask image if needed
    if mask_file is not None:
        mask_image = nibabel.load(mask_file).get_data()
        if mask_image.dtype != 'bool':
            mask_image = mask_image > 0

        if mask_image.shape != image_shape[:len(mask_image.shape)]:
            raise ValueError('Mask should match dimensions if image ({} vs {})'.format(
                mask_image.shape,
                image_shape,
            ))

        test_data = test_data[mask_image][..., 0, :]
    else:
        test_data = test_data.reshape((-1, image_shape[-1]))
        mask_image = None

    test_data = test_data.astype(numpy.float32, copy=False)

    print('Test data size: {}'.format(test_data.shape))

    # Compute probability map if needed
    if probability_file is not None:
        if hasattr(classifier, 'predict_proba'):
            print('Predicting probability maps')
            probability = apply_chunked(classifier.predict_proba, test_data, chunk_size=classifier_chunk_size)

            if number_of_classes is not None and probability.shape[-1] != number_of_classes:
                raise ValueError('Number of classes predicted ({}) and given as command argument ({}) do not match!'.format(probability.shape[-1], number_of_classes))

            if probability.shape[-1] != len(probability_file):
                raise ValueError('Number of probability output files ({}) should match the number of classes ({})!'.format(len(probability_file), probability.shape[-1]))

            print('proba size: {}'.format(probability.shape))
            print('Writing probability maps')
            for class_nr, output_file in enumerate(probability_file):
                print('Reshaping probability map')
                if mask_file is not None:
                    probability_map = numpy.zeros(image_shape[:-2], dtype=numpy.float32)
                    probability_map[mask_image] = probability[:, class_nr]
                else:
                    probability_map = probability[:, class_nr].reshape(image_shape[:-1]).astype(numpy.float32, copy=False)
                print('Writing probability map')
                probability_image = nibabel.Nifti1Image(probability_map, affine)
                nibabel.save(probability_image, output_file)

                # Clear these variables
                del probability_image
                del probability_map
            del probability
        else:
            print('Classifier {} does not have the "predict_proba" method, cannot create probability map'.format(classifier))

    if label_file is not None:
        print('Writing label image')
        if hasattr(classifier, 'predict'):
            print('Predicting label map')
            label_array = apply_chunked(classifier.predict, test_data, chunk_size=classifier_chunk_size)
            print('Reshaping label map (from {})'.format(label_array.shape))
            if mask_image is not None:
                label_image = numpy.zeros(image_shape[:-2], dtype=label_type)
                label_image[mask_image] = label_array
            else:
                label_image = label_array.reshape(image_shape[:-1]).astype(label_type, copy=False)
            print('Writing label map')
            label_image = nibabel.Nifti1Image(label_image, affine)
            nibabel.save(label_image, label_file)
        else:
            print('Classifier {} does not have the "predict" method, cannot create classification label image'.format(classifier))


if __name__ == '__main__':
    main()
