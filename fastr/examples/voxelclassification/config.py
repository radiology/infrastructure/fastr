# Set the debug flag
import os

debug = False

# Avoid type clashes
preferred_types += ["NiftiImageFileCompressed",
                    "NiftiImageFile", 
                    "Float"]

# Select execution plugin
#execution_plugin = "ProcessPoolExecution"
execution_plugin = "LinearExecution"

# Set required mounts
mounts['tmp'] = os.path.join(USER_DIR, 'staging')
mounts['tutorial'] = USER_DIR
