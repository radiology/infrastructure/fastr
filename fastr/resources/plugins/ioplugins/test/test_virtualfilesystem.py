import os
import random
import string
import tempfile

import pytest

import fastr


def random_unique_string(length=8, existing=None):
    """ Return a random alpha-numeric string with a certain length. """
    s = ''.join([random.choice(string.ascii_letters + string.digits) for _ in range(length)])
    if existing is not None and s in existing:
        s = random_unique_string(length, existing)
    return s


@pytest.fixture
def vfs_plugin():
    vfs_plugin = fastr.plugin_manager['VirtualFileSystem']
    return vfs_plugin


@pytest.fixture
def source_dir(fastr_dir):
    source_dir = fastr_dir / "source"
    source_dir.mkdir(exist_ok=True)
    return source_dir


@pytest.fixture
def destination_dir(fastr_dir):
    destination_dir = fastr_dir / "destination"
    destination_dir.mkdir(exist_ok=True)
    return destination_dir


@pytest.fixture
def filename(source_dir):
    _, filename = tempfile.mkstemp(dir=source_dir)
    return os.path.basename(filename)


@pytest.fixture
def niigz_abs_filename(source_dir):
    _, filename = tempfile.mkstemp(dir=source_dir, suffix='.nii.gz')
    return filename


def test_ioplugins_pull_source_data_no_extension(vfs_plugin, destination_dir, filename):
    output_file = os.path.join(destination_dir, filename)
    vfs_plugin.pull_source_data('vfs://tmp/source/{}'.format(filename),
                                destination_dir,
                                'id_0',
                                datatype=fastr.types['NiftiImageFile'])
    assert os.path.isfile(output_file)
    if os.path.isfile(output_file):
        os.remove(output_file)


def test_ioplugins_pull_source_data_niigz(vfs_plugin, destination_dir, filename):
    vfs_plugin.pull_source_data('vfs://tmp/source/{}'.format(filename),
                                destination_dir,
                                'id_nii_0',
                                datatype=fastr.types['NiftiImageFileCompressed'])
    output_file = os.path.join(destination_dir, filename)
    assert os.path.isfile(output_file)
    if os.path.isfile(output_file):
        os.remove(output_file)


def test_ioplugins_pull_source_data_mhd(vfs_plugin, destination_dir):
    absfilename_mhd = os.path.join(fastr.config.mounts['example_data'], 'images', 'mrwhite.mhd')
    absfilename_raw = os.path.join(fastr.config.mounts['example_data'], 'images', 'mrwhite.raw')
    filename_mhd = os.path.basename(absfilename_mhd)
    filename_raw = os.path.basename(absfilename_raw)

    destination_path = destination_dir
    destination_path_mhd = os.path.join(destination_dir, filename_mhd)
    destination_path_raw = os.path.join(destination_dir, filename_raw)

    if os.path.exists(destination_path_mhd):
        os.remove(destination_path_mhd)
    if os.path.exists(destination_path_raw):
        os.remove(destination_path_raw)

    vfs_plugin.pull_source_data(vfs_plugin.path_to_url(absfilename_mhd),
                                destination_path,
                                'id_mhd_0',
                                datatype=fastr.types['ITKImageFile'])

    assert os.path.isfile(destination_path_mhd)
    assert os.path.isfile(destination_path_raw)


def test_ioplugins_push_sink_data_niigz(vfs_plugin, destination_dir, niigz_abs_filename):
    random_filename = random_unique_string()
    output_file = os.path.join(destination_dir, random_filename) + '.nii.gz'
    output_url = vfs_plugin.path_to_url(output_file)
    vfs_plugin.push_sink_data(niigz_abs_filename, output_url)
    assert os.path.isfile(output_file)

    if os.path.isfile(output_file):
        os.remove(output_file)


def test_ioplugins_push_sink_data_mhd(vfs_plugin, destination_dir):
    absfilename_mhd = os.path.join(fastr.config.mounts['example_data'], 'images', 'mrwhite.mhd')
    absfilename_raw = os.path.join(fastr.config.mounts['example_data'], 'images', 'mrwhite.raw')
    filename_mhd = os.path.basename(absfilename_mhd)
    filename_raw = os.path.basename(absfilename_raw)

    push_target = os.path.join(destination_dir, 'sink')
    os.mkdir(push_target)
    destination_path_mhd = os.path.join(push_target, filename_mhd)
    destination_path_raw = os.path.join(push_target, filename_raw)
    destination_url_mhd = vfs_plugin.path_to_url(destination_path_mhd)

    if os.path.exists(destination_path_mhd):
        os.remove(destination_path_mhd)
    if os.path.exists(destination_path_raw):
        os.remove(destination_path_raw)

    vfs_plugin.push_sink_data(absfilename_mhd, destination_url_mhd)
    assert os.path.isfile(destination_path_mhd)
    assert os.path.isfile(destination_path_raw)
