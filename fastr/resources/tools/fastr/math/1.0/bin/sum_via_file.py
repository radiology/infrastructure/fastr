#!/usr/bin/env python

# Copyright 2011-2014 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import json
from pathlib import Path


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Add two numbers')
    parser.add_argument('--in', metavar='IN1', dest='input', type=str, required=True, help='File with values to sum')

    args = parser.parse_args()

    print(f'Loading data from {args.input}')
    data = Path(args.input).read_text()
    print(f'Loaded raw data {data}')
    values = [float(x) for x in data.splitlines()]
    print(f'Parsed values to {values}')

    result = [sum(values)]

    # Show as int if valid int
    result = [int(x) if int(x) == x else x for x in result]

    print('RESULT={}'.format(json.dumps(result)))
