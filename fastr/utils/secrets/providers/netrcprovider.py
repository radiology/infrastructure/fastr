# Copyright 2011-2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from fastr.utils.secrets.secretprovider import SecretProvider
from fastr.utils.secrets.exceptions import CouldNotRetrieveCredentials, NotImplemented

import netrc
import os

class NetrcProvider(SecretProvider):
    def __init__(self):
        if 'netrc' not in globals():
            raise RuntimeError("netrc module required")

    def get_credentials_for_machine(self, machine):
        try:
            netrc_file = os.path.join('~', '_netrc' if os.name == 'nt' else '.netrc')
            netrc_file = os.path.expanduser(netrc_file)
            user, _, password = netrc.netrc(netrc_file).authenticators(machine)
            return {'username': user, 'password': password}
        except TypeError:
            pass # this is caused by netrc.netrc(..).authenticators(..) if no data is found
        raise CouldNotRetrieveCredentials('Could not retrieve login info for "{}" from the .netrc file!'.format(machine))

    def set_credentials_for_machine(self, machine, username, password):
        raise NotImplemented()

    def del_credentials_for_machine(self, machine, username):
        raise NotImplemented()
