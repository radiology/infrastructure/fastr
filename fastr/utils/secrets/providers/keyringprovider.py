# Copyright 2011-2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from fastr.utils.secrets.secretprovider import SecretProvider
from fastr.utils.secrets.exceptions import CouldNotRetrieveCredentials, CouldNotSetCredentials, CouldNotDeleteCredentials


try:
    import keyring
    from keyring.errors import PasswordSetError, PasswordDeleteError
except (ImportError, ValueError) as e:
    pass

class KeyringProvider(SecretProvider):
    def __init__(self):
        if 'keyring' not in globals():
            raise RuntimeError("keyring module required")

    def get_credentials_for_machine(self, machine):
        username = keyring.get_password(machine, 'username')
        password = keyring.get_password(machine, 'password')
        if username is None or password is None:
            raise CouldNotRetrieveCredentials()
        return {'username': username, 'password': password}

    def set_credentials_for_machine(self, machine, username, password):
        try:
            keyring.set_password(machine, 'username', username)
            keyring.set_password(machine, 'password', password)
        except PasswordSetError as e:
            raise CouldNotSetCredentials(str(e))

    def del_credentials_for_machine(self, machine):
        try:
            keyring.delete_password(machine, 'username')
            keyring.delete_password(machine, 'password')
        except PasswordDeleteError as e:
            raise CouldNotDeleteCredentials(str(e))
