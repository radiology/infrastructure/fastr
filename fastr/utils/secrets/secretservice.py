# Copyright 2011-2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from fastr.utils.secrets.providers import KeyringProvider, NetrcProvider
from fastr.utils.secrets.exceptions import CouldNotRetrieveCredentials

class SecretService(object):
    def __init__(self):
        self._providers = self._init_providers()

    def _init_providers(self):
        providers = [KeyringProvider, NetrcProvider]
        result = {}
        for provider in providers:
            try:
                instance = provider()
            except RuntimeError:
                continue
            name = instance.__class__.__name__
            if name.lower().endswith('provider'):
                name = name[:-8]
            result[name] = instance
        return result

    def _check_provider(self, provider_name):
        if provider_name not in self._providers:
            raise ProviderNotFound()

    def list_providers(self):
        return list(self._providers.keys())

    def find_credentials_for_machine(self, machine):
        for provider_name in self.list_providers():
            try:
                result = self._providers[provider_name].get_credentials_for_machine(machine)
                return result
            except CouldNotRetrieveCredentials:
                pass
        raise CouldNotRetrieveCredentials()

    def get_credentials_for_machine(self, provider_name, machine):
        self._check_provider(provider_name)
        return self._providers[provider_name].get_credentials_for_machine(machine)

    def set_credentials_for_machine(self, provider_name, machine, username, password):
        self._check_provider(provider_name)
        return self._providers[provider_name].set_credentials_for_machine(machine, username, password)

    def del_credentials_for_machine(self, machine):
        self._check_provider(provider_name)
        return self._providers[provider_name].del_credentials_for_machine(machine)
