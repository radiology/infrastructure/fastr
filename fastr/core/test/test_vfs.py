# Copyright 2011-2014 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import tempfile
import random
import shutil
import string

import fastr
from fastr import exceptions
from fastr.resources import ioplugins

import pytest


@pytest.fixture
def source_dir(test_mount):
    source_dir = os.path.join(test_mount, 'source')
    os.mkdir(source_dir)

    yield source_dir

    shutil.rmtree(source_dir)


@pytest.fixture
def destination_dir(test_mount):
    destination_dir = os.path.join(test_mount, 'destination')
    os.mkdir(destination_dir)

    yield destination_dir

    shutil.rmtree(destination_dir)


@pytest.fixture
def test_file(source_dir):
    handle, absfilename = tempfile.mkstemp(dir=source_dir)

    return absfilename


@pytest.fixture
def test_nii_file(source_dir):
    handle, absfilename = tempfile.mkstemp(dir=source_dir, suffix='.nii.gz')

    return absfilename


def random_unique_string(length=8, existing=None):
    """ Return a random alpha-numeric string with a certain length. """
    s = ''.join([random.choice(string.ascii_letters + string.digits) for ch in range(length)])
    if existing is not None and s in existing:
        s = random_unique_string(length, existing)
    return s


def test_url_to_path(test_file):
    name = os.path.basename(test_file)
    assert fastr.vfs.url_to_path(f'vfs://test/source/{name}') == test_file


def test_path_to_url(test_file):
    assert fastr.vfs.path_to_url(test_file) == f'vfs://test/source/{os.path.basename(test_file)}'


def test_url_to_path_unknown_scheme():
    # Get a random sequence of strings to get a non existing scheme.
    s = random_unique_string(length=5, existing=list(ioplugins.keys()))
    with pytest.raises(exceptions.FastrUnknownURLSchemeError):
        fastr.vfs.url_to_path("{}://tmp/blaat.nii.gz".format(s))


def test_url_to_path_unknown_mount():
    # Get a random sequence of strings to get a non existing scheme.
    m = random_unique_string(length=5, existing=list(fastr.config.mounts.keys()))
    with pytest.raises(exceptions.FastrMountUnknownError):
        fastr.vfs.url_to_path("vfs://{}/blaat.nii.gz".format(m))


def test_path_to_url_unknown_mount():
    # Get a random sequence of strings to get a non existing scheme.
    m = random_unique_string(length=5, existing=list(fastr.config.mounts.keys()))
    with pytest.raises(exceptions.FastrMountUnknownError):
        fastr.vfs.path_to_url("/{}/blaat.nii.gz".format(m))
